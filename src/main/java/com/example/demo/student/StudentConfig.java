package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student mariam = new Student(
                    "mariam",
                    "email@email.com",
                    LocalDate.of(2001, Month.APRIL, 6)
            );

            Student ivanzolo2004 = new Student(
                    "ivanzolo2004",
                    "email@email.com",
                    LocalDate.of(2001, Month.APRIL, 6)
            );

            repository.saveAll(
                    List.of(
                            mariam, ivanzolo2004
                    )
            );
        };
    }
}
